$(document).ready(function() {
    var q = "a"
        console.log(q)
        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q=" + q,
            datatype: 'json',
            success: function(data) {
                $('#books').html('')
                var result = '<tr>';
                for (var i = 0; i < data.items.length; i++) {
                    if(data.items[i].volumeInfo.imageLinks !== undefined) {
                        if(data.items[i].volumeInfo.imageLinks.smallThumbnail !== undefined) {
                            result += "<tr> <th scope='row' class='align-middle text-center'>" + "<img class='img-fluid' style='width:22vh' src='" + 
                            data.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>" + "</th>" 
                        }
                        else{
                            result += "<tr> <th scope='row' class='align-middle text-center'>" + "No Image" + "</th>" 
                        }
                    }
                    else{
                        result += "<tr> <th scope='row' class='align-middle text-center'>" + "No Image" + "</th>" 
                    }

                    if(data.items[i].volumeInfo.title !== undefined){
                        result += "<td class='align-middle'>" + data.items[i].volumeInfo.title + "</td>"
                    }
                    else{
                        result += "<tr> <th scope='row' class='align-middle text-center'>" + "No Title" + "</th>"
                    }

                    if(data.items[i].volumeInfo.authors !== undefined){
                        result += "<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>"
                    }
                    else{
                        result += "te'>" + "No Author" + "</td>"
                    }

                    if(data.items[i].volumeInfo.publisher !== undefined){
                        result += "<td class='align-middle'>" + data.items[i].volumeInfo.publisher + "</td>"
                    }
                    else{
                        result += "<td class='align-middle'>" + "No Publisher" + "</td>"
                    }
                        
                    if(data.items[i].volumeInfo.publishedDate !== undefined){
                        result += "<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate + "</td>" + "</tr>"
                    }
                    else{
                        result += "<td class='align-middle'>" + "No Published Date" + "</td>"
                    }
                }
                $('#books').append(result);
            },
            error: function(error) {
                alert("Book not found");
            }
        })
    $("#input").on("keyup", function(e) {
        var q = e.currentTarget.value.toLowerCase()
        console.log(q)
        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q=" + q,
            datatype: 'json',
            success: function(data) {
                $('#books').html('')
                var result = '<tr>';
                for (var i = 0; i < data.items.length; i++) {
                    if(data.items[i].volumeInfo.imageLinks !== undefined) {
                        if(data.items[i].volumeInfo.imageLinks.smallThumbnail !== undefined) {
                            result += "<tr> <th scope='row' class='align-middle text-center'>" + "<img class='img-fluid' style='width:22vh' src='" + 
                            data.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>" + "</th>" 
                        }
                        else{
                            result += "<tr> <th scope='row' class='align-middle text-center'>" + "No Image" + "</th>" 
                        }
                    }
                    else{
                        result += "<tr> <th scope='row' class='align-middle text-center'>" + "No Image" + "</th>" 
                    }

                    if(data.items[i].volumeInfo.title !== undefined){
                        result += "<td class='align-middle'>" + data.items[i].volumeInfo.title + "</td>"
                    }
                    else{
                        result += "<tr> <th scope='row' class='align-middle text-center'>" + "No Title" + "</th>"
                    }

                    if(data.items[i].volumeInfo.authors !== undefined){
                        result += "<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>"
                    }
                    else{
                        result += "<td class='align-middle'>" + "No Author" + "</td>"
                    }

                    if(data.items[i].volumeInfo.publisher !== undefined){
                        result += "<td class='align-middle'>" + data.items[i].volumeInfo.publisher + "</td>"
                    }
                    else{
                        result += "<td class='align-middle'>" + "No Publisher" + "</td>"
                    }
                        
                    if(data.items[i].volumeInfo.publishedDate !== undefined){
                        result += "<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate + "</td>" + "</tr>"
                    }
                    else{
                        result += "<td class='align-middle'>" + "No Published Date" + "</td>"
                    }
                        
                }
                $('#books').append(result);
            },
            error: function(error) {
                alert("Book not found");
            }
        })
    });
});
