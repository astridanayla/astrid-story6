from django.test import TestCase, Client, LiveServerTestCase
from django.urls import reverse, resolve
from .views import booksearch_view
import datetime, time

from selenium import webdriver
from selenium.webdriver.chrome.options import Options

# Create your tests here. janlup uncomment test yg lain
class BookSearchTest(TestCase):
    def test_apakah_url_landing_ada(self):
        response = Client().get('/book-search/')
        self.assertEqual(response.status_code, 200)

    def test_url_landing_func(self):
        found = resolve('/book-search/')
        self.assertEqual(found.func, booksearch_view)

    def test_redirect(self):
        response = Client().get('/book-search/')
        self.assertEqual(response.status_code, 200)

    def test_apakah_html_sesuai(self):
        response = Client().get('/book-search/')
        self.assertTemplateUsed(response, 'booksearch/booksearch.html')
    
    def test_apakah_ada_judul(self):
        name = "Book Search."
        response = Client().get('/book-search/')
        html_response = response.content.decode('utf8')
        self.assertIn(name, html_response)

    def test_url_json(self):
        response = Client().get('/book-search/data/')
        self.assertEqual(response.status_code,200)

class BookSearchFunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        self.chrome_options = Options()
        self.chrome_options.add_argument('--no-sandbox')
        self.chrome_options.add_argument('--headless')
        self.chrome_options.add_argument('--disable-gpu')
        self.chrome_path = './chromedriver'
        self.browser = webdriver.Chrome(options=self.chrome_options, executable_path=self.chrome_path)

    def tearDown(self):
        self.browser.quit()
        super().tearDown()

    def test_apakah_ada_table_head(self):
        self.browser.get(self.live_server_url + '/book-search/')
        table = self.browser.find_element_by_tag_name('th').text
        self.assertIn('Cover', table)

    def test_apakah_hasil_search_ditampilkan(self):
        self.browser.get(self.live_server_url + '/book-search/')
        time.sleep(8)

        inputan = self.browser.find_element_by_class_name('inputan')
        inputan.send_keys('a')

        time.sleep(8)
        
        self.assertIn("a", self.browser.page_source)

