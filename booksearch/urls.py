from django.urls import path
from . import views
from .views import booksearch_view, data

appname = 'booksearch'

urlpatterns = [
    path('', booksearch_view, name='bookview'),
    path('data/', data, name='bookdata')
]