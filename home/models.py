from django.db import models
import datetime

# Create your models here.
class UserInput(models.Model):
    date =  models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length = 300, blank=False)
