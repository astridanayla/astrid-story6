from django.apps import AppConfig

class HomeConfig(AppConfig):
    name = 'home'

class AboutConfig(AppConfig):
    name = 'about'

class BookSearchConfig(AppConfig):
    name = 'booksearch'
