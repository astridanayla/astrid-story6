from django.shortcuts import render, redirect
from .models import UserInput
from .forms import InputForm

# Create your views here.
def home_view(request):
    if request.method == 'POST':
        form = InputForm(request.POST)
        if(form.is_valid):
            inputan = UserInput()
            if 'tuliskan_Status' in request.POST:
                inputan.status = request.POST['tuliskan_Status']
            inputan.save()
        form = InputForm()
        status = UserInput.objects.all()
        context = {
            'formulir' : form,
            'status' : status,
        }
        return render(request, 'home/home.html', context)
    else:
        form = InputForm()
        status = UserInput.objects.all()
        context = {
            'formulir' : form,
            'status' : status,
        }
        return render(request, 'home/home.html', context)