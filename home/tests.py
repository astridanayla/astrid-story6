from django.test import TestCase, Client, LiveServerTestCase
from django.urls import reverse, resolve
from .views import home_view
from .models import UserInput
from .forms import InputForm
import datetime

from selenium import webdriver
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class LandingTest(TestCase):
    def test_apakah_url_landing_ada(self):
        response = Client().get('/home/')
        self.assertEqual(response.status_code, 200)

    def test_url_landing_func(self):
        found = resolve('/home/')
        self.assertEqual(found.func, home_view)

    def test_redirect(self):
        response = Client().get('/home/')
        self.assertEqual(response.status_code, 200)

    def test_apakah_html_sesuai(self):
        response = Client().get('/home/')
        self.assertTemplateUsed(response, 'home/home.html')

    def test_apakah_model_bisa_bikin_objek_baru(self):
        new_input = UserInput.objects.create(
            status = "test status"
        )
        hitung_semua_input = UserInput.objects.all().count()
        self.assertEqual(hitung_semua_input, 1)

    def test_apakah_status_yang_disimpan_sesuai(self):
        new_input = self.client.post('/home/', data={'tuliskan_Status': 'tes status'})
        field_name = 'status'
        obj = UserInput.objects.first()
        field_value = getattr(obj, field_name)
        self.assertEqual("tes status", field_value)

    def test_apakah_post_status_sukses(self):
        test = "test status"
        input_post = Client().post('/home/', {'tuliskan_Status': test})
        self.assertEqual(input_post.status_code, 200)
    
    def test_form_validation_field_kosong(self):
        form = InputForm(data = {'tuliskan_Status': ""})
        self.assertFalse(form.is_valid())
        self.assertEqual( form.errors['tuliskan_Status'], ["This field is required."])
        
    
class LandingFunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        self.chrome_options = Options()
        self.chrome_options.add_argument('--no-sandbox')
        self.chrome_options.add_argument('--headless')
        self.chrome_options.add_argument('--disable-gpu')
        self.chrome_path = './chromedriver'
        self.browser = webdriver.Chrome(options=self.chrome_options, executable_path=self.chrome_path)

    def tearDown(self):
        self.browser.quit()
        super().tearDown()

    def test_bisa_masukin_status_dan_status_muncul(self):
        self.browser.get(self.live_server_url + '/home/')

        status = self.browser.find_element_by_id('id_tuliskan_Status')
        status.send_keys('Coba Coba')
        status.submit()

        status_input = self.browser.find_element_by_class_name('statusnya').text
        self.assertEqual('Coba Coba', status_input)



    