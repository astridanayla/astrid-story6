from django import forms
from .models import UserInput

class InputForm(forms.Form):
    tuliskan_Status = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Tuliskan statusmu di sini!',
        'type' : 'text',
        'required' : True
    }))