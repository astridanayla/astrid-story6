from django.test import TestCase, Client, LiveServerTestCase
from django.urls import reverse, resolve
from .views import about_view
import datetime, time

from selenium import webdriver
from selenium.webdriver.chrome.options import Options


# Create your tests here.
class LandingTest(TestCase):
    def test_apakah_url_landing_ada(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_landing_func(self):
        found = resolve('/')
        self.assertEqual(found.func, about_view)

    def test_redirect(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_apakah_html_sesuai(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'about/about.html')
    
    def test_apakah_ada_judul(self):
        name = "about me."
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(name, html_response)

class LandingFunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        self.chrome_options = Options()
        self.chrome_options.add_argument('--no-sandbox')
        self.chrome_options.add_argument('--headless')
        self.chrome_options.add_argument('--disable-gpu')
        self.chrome_path = './chromedriver'
        self.browser = webdriver.Chrome(options=self.chrome_options, executable_path=self.chrome_path)

    def tearDown(self):
        self.browser.quit()
        super().tearDown()
    
    def test_ada_accordion_activity(self):
        self.browser.get(self.live_server_url)
        accordion = self.browser.find_element_by_id("activity").text
        self.assertEquals(accordion, "Activity")

    def test_ada_accordion_organization(self):
        self.browser.get(self.live_server_url)
        accordion = self.browser.find_element_by_id("organization").text
        self.assertEquals(accordion, "Organizations/Volunteers")

    def test_ada_accordion_achievement(self):
        self.browser.get(self.live_server_url)
        accordion = self.browser.find_element_by_id("achievement").text
        self.assertEquals(accordion, "Achievement")

    def test_konten_accordion_activity(self):
        self.browser.get(self.live_server_url)
        time.sleep(2)
        self.browser.find_element_by_xpath('//*[@id="activity"]').click()
        self.assertIn("A computer science student", self.browser.page_source)
        time.sleep(2)

    def test_konten_accordion_organization(self):
        self.browser.get(self.live_server_url)
        time.sleep(2)
        self.browser.find_element_by_xpath('//*[@id="organization"]').click()
        self.assertIn("Staff of Visual Design Division", self.browser.page_source)
        time.sleep(2)

    def test_konten_accordion_achievement(self):
        self.browser.get(self.live_server_url)
        time.sleep(2)
        self.browser.find_element_by_xpath('//*[@id="achievement"]').click()
        self.assertIn("ISAFIS Model United Nations 2015", self.browser.page_source)
        time.sleep(2)

    def test_ada_button_switch_theme(self):
        self.browser.get(self.live_server_url)
        button_tema = self.browser.find_element_by_id("switch").text
        self.assertEquals(button_tema, "Change Theme")
    
    def test_ganti_tema(self):
        self.browser.get(self.live_server_url)
        time.sleep(2)

        body_background = self.browser.find_element_by_tag_name('body').value_of_css_property('background')
        self.assertIn('rgb(0, 96, 74)', body_background)
        time.sleep(2)

        self.browser.find_element_by_class_name('theme-switch').click()
        body_background = self.browser.find_element_by_tag_name('body').value_of_css_property('background')
        self.assertIn('rgb(255, 165, 63)', body_background)