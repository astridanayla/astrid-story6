from django.urls import path
from . import views
from .views import about_view

appname = 'about'

urlpatterns = [
    path('', about_view, name='aboutview')
]