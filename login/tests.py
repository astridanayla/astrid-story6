from django.test import TestCase
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import reverse, resolve
from .views import login_view, signup_view, logout_request
from django.contrib.auth.models import User
from .forms import LoginForm, SignupForm
from django.http import HttpRequest
import datetime, time

from selenium import webdriver
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class SignInTest(TestCase):
    def test_apakah_url_signin_ada(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_apakah_url_signout_bekerja(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 302)

    def test_url_signup_func(self):
        found = resolve('/login/')
        self.assertEqual(found.func, login_view)

    def test_apakah_html_sesuai(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login/login.html')

    def test_apakah_ada_header(self):
        request = HttpRequest()
        response = login_view(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Please sign in.", html_response)

class SignUpTest(TestCase):
    def test_apakah_url_signup_ada(self):
        response = Client().get('/signup/')
        self.assertEqual(response.status_code, 200)

    def test_apakah_url_signout_bekerja(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 302)

    def test_url_signup_func(self):
        found = resolve('/signup/')
        self.assertEqual(found.func, signup_view)

    def test_apakah_html_sesuai(self):
        response = Client().get('/signup/')
        self.assertTemplateUsed(response, 'login/signup.html')

    def test_apakah_ada_header(self):
        request = HttpRequest()
        response = signup_view(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Please sign up.", html_response)

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        self.chrome_options = Options()
        self.chrome_options.add_argument('--no-sandbox')
        self.chrome_options.add_argument('--headless')
        self.chrome_options.add_argument('--disable-gpu')
        self.chrome_path = './chromedriver'
        self.browser = webdriver.Chrome(options=self.chrome_options, executable_path=self.chrome_path)

    def tearDown(self):
        self.browser.quit()
        super().tearDown()

    def test_bisa_bikin_akun(self):
        self.browser.get(self.live_server_url + '/signup/')
        time.sleep(2)

        nama_depan = self.browser.find_element_by_xpath('//*[@id="id_first_name"]')
        nama_depan.send_keys('astrida')

        nama_belakang = self.browser.find_element_by_xpath('//*[@id="id_last_name"]')
        nama_belakang.send_keys('nayla')

        username = self.browser.find_element_by_xpath('//*[@id="id_username"]')
        username.send_keys('astridanayla')

        pass1 = self.browser.find_element_by_xpath('//*[@id="id_password1"]')
        pass1.send_keys('astridcakepbanget')

        pass2 = self.browser.find_element_by_xpath('//*[@id="id_password2"]')
        pass2.send_keys('astridcakepbanget')

        button = self.browser.find_element_by_xpath('//*[@id="formnya"]/div/div/input')
        button.click()

        time.sleep(8)
        self.assertIn("Have a nice day,", self.browser.page_source)
        self.assertIn("astrida", self.browser.page_source)

        logout = self.browser.find_element_by_xpath('/html/body/div[1]/form/input')
        logout.click()
        time.sleep(8)

        username_box = self.browser.find_element_by_xpath('//*[@id="id_username"]')
        username_box.send_keys('astridanayla')

        password_box = self.browser.find_element_by_xpath('//*[@id="id_password"]')
        password_box.send_keys('astridcakepbanget')

        button_login = self.browser.find_element_by_xpath('//*[@id="button_signin"]')
        button_login.click()

        time.sleep(8)
        self.assertIn("Have a nice day,", self.browser.page_source)
        self.assertIn("astrida", self.browser.page_source)