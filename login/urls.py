from django.urls import path
from . import views
from .views import login_view, signup_view

appname = 'login'

urlpatterns = [
    path('login/', login_view, name='login'),
    path('signup/', signup_view, name='signup'),
    path('logout/', views.logout_request, name='logout'),
]