from django.shortcuts import render, redirect
from .forms import SignupForm, LoginForm
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout, authenticate
from django.contrib import messages

# Create your views here.
def login_view(request):
    if request.method == 'POST':
        form = LoginForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
            else:
                messages.error(request, 'Invalid entry')
        else:
            messages.error(request, 'Invalid entry')
    else:
        form = LoginForm()
    first_name = "astrida"
    last_name = "nayla"
    context = {
        'first' : first_name,
        'last' : last_name,
        'form' : form
    }
    return render(request, 'login/login.html', context)

def signup_view(request):
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect("login")
        else:
            for msg in form.error_messages:
                messages.error(request, 'Invalid entry')
    else:
        form = SignupForm()
    context = {
        'form' : form
    }
    return render(request, 'login/signup.html', context)

def logout_request(request):
    logout(request)
    return redirect("login")